package ru.edu;

import ru.edu.model.Athlete;
import ru.edu.model.AthleteImpl;
import ru.edu.model.CountryParticipant;
import ru.edu.model.Participant;

import java.util.*;
import java.util.function.BiFunction;

/**
 * Реализация интерфейса Competition.
 *
 * В данном классе происходят различные события, связанные с соревнованиями.
 */
public class CompetitionImpl implements Competition{

    /**
     * Начальное значение id, равное 0.
     * Значение будет инкрементироваться при добавлении нового участника.
     */
    private long id = 0;

    /**
     * Мапа - словарь всех участников соревнования.
     */
    private Map<Long, ParticipantImpl> participantMap = new HashMap<>();

    /**
     * Список зарегистрированных атлетов.
     */
    private Set<Athlete> registeredAthlete = new HashSet<>();

    /**
     * Мапа - словарь всех стран соревнования.
     * Со странами также связаны списки их участников и счет стран.
     */
    private Map<String, CountryParticipantImpl> countryMap = new HashMap<>();



    /**
     * Регистрация участника.
     * Здесь мы напрямую не отдаем наружу участника (internalAndSecuredParticipant).
     * Мы скрываем этот заведенный объект, отдавая наружу его клон (копию).
     *
     * Регистрируем нового участника, если ранее он не был зарегистрирован.
     * Также, если страна участника не была в списках, мы добавляем страну в список стран-участниц.
     * Если страна была в списке стран-участниц,
     * мы просто добавляем участника в список его страны (в массив страны).
     *
     *
     * @param participant - участник
     * @return зарегистрированный участник
     * @throws IllegalArgumentException - при попытке повторной регистрации
     */
    @Override
    public Participant register(Athlete participant) {
        if (registeredAthlete.contains(participant)) {
            throw new IllegalArgumentException("Duplicate registration isn't allowed!");
        }
        registeredAthlete.add(participant);
        ParticipantImpl internalAndSecuredParticipant = new ParticipantImpl(++id, participant);
        participantMap.put(internalAndSecuredParticipant.id, internalAndSecuredParticipant);

        if (!countryMap.containsKey(participant.getCountry())) {
            CountryParticipantImpl countryParticipantImpl =
                    new CountryParticipantImpl(participant.getCountry());
            countryParticipantImpl.addCountryParticipant(internalAndSecuredParticipant);

            countryMap.put(participant.getCountry(), countryParticipantImpl);
        } else {
            countryMap.get(participant.getCountry())
                    .addCountryParticipant(internalAndSecuredParticipant);
        }

        return internalAndSecuredParticipant.getClone();
    }

    /**
     * Обновление счета участника по его id.
     * Требуется константное время выполнения
     * <p>
     * updateScore(10) прибавляет 10 очков
     * updateScore(-5) отнимает 5 очков
     *
     * @param id    регистрационный номер участника
     * @param score +/- величина изменения счета
     */
    @Override
    public void updateScore(long id, long score) {
        ParticipantImpl participantForScoreUpdate = participantMap.get(id);
        if (participantForScoreUpdate != null) {
            participantForScoreUpdate.modifyScore(score);

            BiFunction<String, CountryParticipantImpl, CountryParticipantImpl> computeFunction =
                    (String key, CountryParticipantImpl oldValue) -> {

                oldValue.score = oldValue.score + score;
                return oldValue;
            };
            CountryParticipantImpl result = countryMap.computeIfPresent(participantMap.get(id)
                    .getAthlete().getCountry(), computeFunction);
        }else {
            throw new IllegalArgumentException("Invalid ID has been typed");
        }

    }

    /**
     * Обновление счета участника по его объекту Participant
     * Требуется константное время выполнения
     *
     * @param participant - зарегистрированный участник
     * @param score -      новое значение счета
     */
    @Override
    public void updateScore(Participant participant, long score) {
        updateScore(participant.getId(), score);
    }

    /**
     * Получение результатов.
     * Сортировка участников от большего счета к меньшему.
     *
     * @return отсортированный список участников
     */
    @Override
    public List<Participant> getResults() {
        List<Participant> participantsSortedAccToResult = new LinkedList<>(participantMap.values());

        participantsSortedAccToResult.sort(((o1, o2)  -> {
            if (o1.getScore() == o2.getScore()) {
                return 0;
            }

            return (o1.getScore() < o2.getScore()) ? 1 : -1;
        }));

        return participantsSortedAccToResult;
    }

    /**
     * Получение результатов по странам.
     * Группировка участников из одной страны и сумма их счетов.
     * Сортировка результатов от большего счета к меньшему.
     *
     * @return отсортированный список стран-участников
     */
    @Override
    public List<CountryParticipant> getParticipantsCountries() {

        List<CountryParticipant> countryParticipants = new LinkedList<>(countryMap.values());

        countryParticipants.sort(((o1, o2)  -> {
            if (o1.getScore() == o2.getScore()) {
                return 0;
            }

            return (o1.getScore() < o2.getScore()) ? 1 : -1;
        }));

        return countryParticipants;
    }


    /**
     * Реализация интерфейса Participant.
     * Данный класс позволяет работать с участниками соревнования.
     *
     * Здесь мы реализуем методы интерфейса Participant, а также создаем свои методы и конструктор.
     */
    private static class ParticipantImpl implements Participant {

        /**
         * Регистрационный номер участника соревнования.
         */
        public final Long id;

        /**
         * Атлет, проходящий регистрацию.
         */
        private final Athlete athlete;

        /**
         * Счет участника соревнования.
         */
        private long score;

        /**
         * Конструктор для участника соревнования.
         *
         * @param id - номер участника.
         * @param athlete - участвующий атлет.
         */
        public ParticipantImpl(long id, Athlete athlete) {
            this.id = id;
            this.athlete = athlete;
        }

        /**
         * Получение информации о регистрационном номере.
         *
         * @return регистрационный номер
         */
        @Override
        public Long getId() {
            return id;
        }

        /**
         * Информация об атлете.
         *
         * @return объект спортсмена
         */
        @Override
        public Athlete getAthlete() {
            return athlete;
        }

        /**
         * Счет участника.
         *
         * @return счет
         */
        @Override
        public long getScore() {
            return score;
        }

        /**
         * Изменение (обновление) счета участника соревнования.
         *
         * @param value - количество баллов, на которые нужно увеличить/уменьшить счет
         */
        public void modifyScore(long value) {
            score += value;
        }

        /**
         * Создание клона (копии) участника соревнования.
         *
         * @return - клон (копия) участника соревнования.
         */
        public ParticipantImpl getClone() {
            return new ParticipantImpl(this.id, this.athlete);
        }

        @Override
        public String toString() {
            return "ParticipantImpl{" +
                    "id=" + id +
                    ", athlete=" + athlete +
                    ", score=" + score +
                    '}';
        }
    }

    /**
     * Реализация интерфейса CountryParticipant.
     * Данный класс позволяет работать со странами-участницами.
     *
     * Здесь мы реализуем методы интерфейса CountryParticipant,
     * а также создаем свои методы и конструктор.
     */
    private static class CountryParticipantImpl implements CountryParticipant {

        /**
         * Название страны.
         */
        final String name;

        /**
         * Список участников страны.
         */
        private List<Participant> participants;

        /**
         * Счет страны.
         */
        private long score;

        /**
         * Конструктор страны-участницы и
         * ассоциированных с ней полей и объектов.
         *
         * @param name - название страны участника
         */
        CountryParticipantImpl(String name) {
            this.name = name;
            this.participants = new ArrayList<>();
            this.score = 0;
        }

        /**
         * Название страны.
         *
         * @return название
         */
        @Override
        public String getName() {
            return name;
        }

        /**
         * Добавление нового участника в список его страны.
         *
         * @param participant - участник соревнований
         */
        public void addCountryParticipant(Participant participant) {
            this.participants.add(participant);
        }

        /**
         * Список участников страны.
         *
         * @return список участников
         */
        @Override
        public List<Participant> getParticipants() {
            return participants;
        }

        /**
         * Счет страны.
         *
         * @return счет
         */
        @Override
        public long getScore() {
            return score;
        }

        /**
         * Вывод информации о странах-участницах соревнования.
         *
         * @return данные
         */
        @Override
        public String toString() {
            return "CountryParticipantImpl{" +
                    "name='" + name + '\'' +
                    ", participants=" + participants +
                    ", score=" + score +
                    '}';
        }
    }
}
