package ru.edu.model;

import java.util.Objects;

public class AthleteImpl implements Athlete{

    /**
     * Имя атлета.
     */
    private String firstName;

    /**
     * Фамилия атлета.
     */
    private String lastName;

    /**
     * Страна атлета.
     */
    private String country;

    public static Builder builder() {
        return new Builder();
    }

    /**
     * Статический вложенный класс Builder.
     *
     * Конструктор атлетов.
     */
    public static class Builder {
        private final AthleteImpl obj = new AthleteImpl();

        public Builder setFirstName(String firstName) {
            obj.firstName = firstName;
            return this;
        }

        public Builder setLastName(String lastName) {
            obj.lastName = lastName;
            return this;
        }

        public Builder setCountry(String country) {
            obj.country = country;
            return this;
        }

        public AthleteImpl build() {
            if (obj.firstName == null || obj.lastName == null || obj.country == null) {
                throw new IllegalArgumentException("First name and last" +
                        "name and country are required");
            }
            return obj;
        }
    }

    /**
     * Имя.
     *
     * @return значение
     */
    @Override
    public String getFirstName() {
        return firstName;
    }

    /**
     * Фамилия.
     *
     * @return значение
     */
    @Override
    public String getLastName() {
        return lastName;
    }

    /**
     * Страна.
     *
     * @return значение
     */
    @Override
    public String getCountry() {
        return country;
    }

    /**
     * Переопределенный метод equals().
     *
     * @param obj - передаваемый объект-атлет для проверки на equals()
     * @return - результат equals()
     */
    @Override
    public boolean equals(Object obj) {

        if (obj == null) {
            return false;
        }

        if (this == obj) {
            return  true;
        }

        if (!(this.getClass().equals(obj.getClass()))) {
            return false;
        }

        AthleteImpl other = (AthleteImpl) obj;

        return firstName.equals(other.firstName)
                && lastName.equals(other.lastName)
                && country.equals(other.country);
    }

    /**
     * Переопределенный метод hashCode().
     *
     * @return - hash-код.
     */
    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, country);
    }

    /**
     * Переопределенный метод toString().
     *
     * @return - строковое представление объекта
     */
    @Override
    public String toString() {
        return "AthleteImpl{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}
