package ru.edu.model;

import org.junit.Before;
import org.junit.Test;
import ru.edu.CompetitionImpl;

import static org.junit.Assert.*;

public class AthleteImplTest {

    public static final String TEST_NAME = "TestName";
    public static final String TEST_COUNTRY = "TestCountry";
    AthleteImpl firstAthlete, secondAthlete, thirdAthlete, fourthAthlete, fifthAthlete, testAthlete, testAthlete2, testAthlete3;

    CompetitionImpl competition = new CompetitionImpl();

    AthleteImpl athlete = new AthleteImpl();

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void builder() {

        firstAthlete = AthleteImpl.builder()
                .setFirstName("Evgenii")
                .setLastName("Boyko")
                .setCountry("Russia")
                .build();

        secondAthlete = AthleteImpl.builder()
                .setFirstName("Arnold")
                .setLastName("Schwarzenegger")
                .setCountry("Oesterreich")
                .build();

        thirdAthlete = AthleteImpl.builder()
                .setFirstName("Allois")
                .setLastName("Schwarzenegger")
                .setCountry("Oesterreich")
                .build();

        fourthAthlete = AthleteImpl.builder()
                .setFirstName("UnknownFirstName")
                .setLastName("UnknownLastName")
                .setCountry("UnknownCountry")
                .build();

        fifthAthlete = AthleteImpl.builder()
                .setFirstName("Yurii")
                .setLastName("Vlasov")
                .setCountry("USSR")
                .build();

        testAthlete = AthleteImpl.builder()
                .setFirstName("Evgenii")
                .setLastName("Vlasov")
                .setCountry("Russia")
                .build();

        testAthlete2 = AthleteImpl.builder()
                .setFirstName("Evgenii")
                .setLastName("Vlasov")
                .setCountry("Russia")
                .build();

        testAthlete3 = AthleteImpl.builder()
                .setFirstName("")
                .setLastName("")
                .setCountry("")
                .build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAthleteWithoutData() {

        AthleteImpl.builder()
                .setFirstName(TEST_NAME)
                .setCountry(TEST_COUNTRY)
                .build();
    }

    @Test
    public void getFirstName() {
        builder();
        assertEquals("Evgenii", firstAthlete.getFirstName());
    }

    @Test
    public void getLastName() {
        builder();
        assertEquals("Vlasov", fifthAthlete.getLastName());
    }

    @Test
    public void getCountry() {
        builder();
        assertEquals("UnknownCountry", fourthAthlete.getCountry());
    }

    @Test
    public void testEquals() {
        builder();
        assertFalse(equals(null));
        assertFalse(equals(2));

        assertTrue(secondAthlete.equals(firstAthlete = AthleteImpl.builder()
                .setFirstName("Arnold")
                .setLastName("Schwarzenegger")
                .setCountry("Oesterreich")
                .build()));

        athlete.equals(null);
        athlete.equals(2);
        athlete.equals(athlete);
    }

    @Test
    public void testHashCode() {
        builder();
        assertNotNull(hashCode());

        String str = "TEST";

        System.out.println(hashCode());
        assertEquals(testAthlete.hashCode(), testAthlete2.hashCode());
        assertNotEquals(testAthlete.hashCode(), str.hashCode());

    }

    @Test
    public void testToString() {
        builder();
        System.out.println(firstAthlete);
    }
}