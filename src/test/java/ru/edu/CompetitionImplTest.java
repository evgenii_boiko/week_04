package ru.edu;

import org.junit.Before;
import org.junit.Test;
import ru.edu.model.AthleteImpl;
import ru.edu.model.CountryParticipant;
import ru.edu.model.Participant;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

public class CompetitionImplTest {

    AthleteImpl firstAthlete, secondAthlete, thirdAthlete, fourthAthlete, fifthAthlete, sixthAthlete, seventhAthlete;

    CompetitionImpl competition = new CompetitionImpl();

    @Test
    public void builder() {

        firstAthlete = AthleteImpl.builder()
                .setFirstName("Evgenii")
                .setLastName("Boyko")
                .setCountry("Russia")
                .build();

        secondAthlete = AthleteImpl.builder()
                .setFirstName("Arnold")
                .setLastName("Schwarzenegger")
                .setCountry("Oesterreich")
                .build();

        thirdAthlete = AthleteImpl.builder()
                .setFirstName("Allois")
                .setLastName("Schwarzenegger")
                .setCountry("Oesterreich")
                .build();

        fourthAthlete = AthleteImpl.builder()
                .setFirstName("UnknownFirstName")
                .setLastName("UnknownLastName")
                .setCountry("UnknownCountry")
                .build();

        fifthAthlete = AthleteImpl.builder()
                .setFirstName("Yurii")
                .setLastName("Vlasov")
                .setCountry("USSR")
                .build();

        sixthAthlete = AthleteImpl.builder()
                .setFirstName("Anatolii")
                .setLastName("Osipov")
                .setCountry("USSR")
                .build();

        seventhAthlete = AthleteImpl.builder()
                .setFirstName("Victor")
                .setLastName("Petrov")
                .setCountry("Russia")
                .build();
    }

    @Test
    public void register() {
        builder();
        competition.register(firstAthlete);
        competition.register(secondAthlete);
        competition.register(thirdAthlete);
        competition.register(fourthAthlete);
        competition.register(fifthAthlete);
        competition.register(sixthAthlete);
        competition.register(seventhAthlete);
    }

    @Test (expected = IllegalArgumentException.class)
    public void duplicateRegistrationTest() {
        builder();
        competition.register(firstAthlete);
        competition.register(firstAthlete);
    }

    @Test (expected = IllegalArgumentException.class)
    public void updateScoreInvalidId() {
        builder();
        competition.register(firstAthlete);
        competition.updateScore(500,10);
    }

    @Test
    public void updateScore() {
        builder();
        competition.register(firstAthlete);
        competition.register(secondAthlete);
        competition.register(thirdAthlete);
        competition.register(fourthAthlete);
        competition.register(fifthAthlete);
        competition.register(sixthAthlete);
        competition.register(seventhAthlete);

        System.out.println(competition.getResults());
        System.out.println("+++++++++++++++++++++++++");
        competition.updateScore(2,10);
        System.out.println(competition.getResults());
        System.out.println("+++++++++++++++++++++++++");
        System.out.println(competition.getResults().get(2));
        assertEquals(10, competition.getResults().get(0).getScore());
        System.out.println("+++++++++++++++++++++++++");
        System.out.println("+++++++++++++++++++++++++");
        System.out.println(":::::::::::::::::::::::::::Additional Tests:::::::::::::::::::::::::::");
        competition.updateScore(3,50);
        System.out.println(competition.getResults());
        assertEquals(50, competition.getResults().get(0).getScore());
        assertEquals(10, competition.getResults().get(1).getScore());
        System.out.println("+++++++++++++++++++++++++");
        competition.updateScore(1,-50);
        System.out.println(competition.getResults());
        assertEquals(0, competition.getResults().get(2).getScore());
        assertEquals(10, competition.getResults().get(1).getScore());
        System.out.println("+++++++++++++++++++++++++");
        competition.updateScore(6,-6000);
        System.out.println(competition.getResults());
        assertEquals(-6000, competition.getResults().get(6).getScore());
        System.out.println("+++++++++++++++++++++++++");
        System.out.println("+++++++++++++++++++++++++");
        System.out.println(":::::::::::::::::::::::::::Additional Tests with transfer PARTICIPANT to updateScore() method:::::::::::::::::::::::::::");
        competition.updateScore(competition.getResults().get(6),12000);
        System.out.println(competition.getResults());
        assertEquals(6000, competition.getResults().get(0).getScore());
    }

    @Test
    public void getResults() {
        builder();
        competition.register(firstAthlete);
        competition.register(secondAthlete);
        competition.register(thirdAthlete);
        competition.register(fourthAthlete);
        competition.register(fifthAthlete);
        competition.register(sixthAthlete);
        competition.register(seventhAthlete);
        System.out.println(competition.getResults());
        assertNotNull(competition.getResults());
    }

    @Test
    public void getParticipantsCountries() {
        builder();
        competition.register(firstAthlete);
        competition.register(secondAthlete);
        competition.register(thirdAthlete);
        competition.register(fourthAthlete);
        competition.register(fifthAthlete);
        competition.register(sixthAthlete);
        competition.register(seventhAthlete);

        System.out.println(competition.getParticipantsCountries());
        assertNotNull(competition.getParticipantsCountries());

        System.out.println("+++++++++++++++++++++++++");
        competition.updateScore(6,10);
        System.out.println(competition.getParticipantsCountries());
        System.out.println(competition.getParticipantsCountries().get(0).getParticipants().get(0).getAthlete().getCountry());
        assertEquals("USSR", competition.getParticipantsCountries().get(0).getParticipants().get(0).getAthlete().getCountry());

        competition.getParticipantsCountries().get(3).getName();
    }
}